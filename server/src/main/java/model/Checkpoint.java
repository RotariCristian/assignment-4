package model;

import DAO.UserDAO;
import DAO.UserDAOImp;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Checkpoint  {

    @GeneratedValue
    @Id
    private int id;

    @OneToOne
    private Package aPackage;

    @Column
    private String city;

    @Column
    private Date date;

    public Checkpoint() {
    }

    public Checkpoint(Package aPackage, String city, Date date) {
        this.aPackage = aPackage;
        this.city = city;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDate() {
        return date;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
