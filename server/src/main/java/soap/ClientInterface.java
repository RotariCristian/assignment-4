package soap;

import model.Checkpoint;
import model.Package;
import model.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.List;

@WebService
@SOAPBinding
public interface ClientInterface {

    @WebMethod
    User getUserByCredentials(String username, String password);

    @WebMethod
    Package[] getUserPackages(User user);

    @WebMethod
    Package getPackageById(Integer id, User user);

    @WebMethod
    Checkpoint[] getPackageRoute(Integer packageId, User user);

    @WebMethod
    User createUser(String username, String password);
}
