package soap;

import model.Package;
import model.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding
public interface AdminInterface {

    @WebMethod
    Package[] getAllPackages(User user);

    @WebMethod
    void deletePackage(Integer id, User user);

    @WebMethod
    void createPackage(String name, String description, String senderCity,
                            String destinationCity, String sender, String receiver, User user);

    @WebMethod
    void addCheckpoint(Integer packId, String checkpoint, User user);
}
