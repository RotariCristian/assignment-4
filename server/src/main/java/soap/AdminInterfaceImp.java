package soap;

import DAO.CheckpointDAOImp;
import DAO.PackageDAOImp;
import DAO.UserDAOImp;
import model.Checkpoint;
import model.Package;
import model.User;
import services.PackageService;
import services.PackageServiceImpl;
import services.UserService;
import services.UserServiceImpl;

import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@WebService(endpointInterface = "soap.AdminInterface")
public class AdminInterfaceImp implements AdminInterface {

    private UserService userService = new UserServiceImpl(new UserDAOImp());

    @Override
    public Package[] getAllPackages(User user) {
        if(!user.isAdmin() || !userService.isLoggedIn(user)) return new Package[0];
        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(),  new CheckpointDAOImp());
        List<Package> packages = packageService.getAllPackages();
        return packages.toArray(new Package[packages.size()]);
    }

    @Override
    public void deletePackage(Integer id, User user) {
        if(!user.isAdmin() || !userService.isLoggedIn(user)) return;

        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(),  new CheckpointDAOImp());
        packageService.deletePackageById(id);

    }

    @Override
    public void createPackage(String name, String description, String senderCity, String destinationCity,
                                String sender, String receiver, User user) {
        if(!user.isAdmin() || !userService.isLoggedIn(user)) return;
        Package pack = new Package();
        pack.setDescription(description);
        pack.setDestinationCity(destinationCity);
        pack.setName(name);
        pack.setSenderCity(senderCity);
        pack.setSender(userService.getUserByUsername(sender));
        pack.setReceiver(userService.getUserByUsername(receiver));
        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(),  new CheckpointDAOImp());
        packageService.addPackage(pack);
    }

    @Override
    public void addCheckpoint(Integer packId, String checkpoint, User user) {
        if(!user.isAdmin() || !userService.isLoggedIn(user)) return;

        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(), new CheckpointDAOImp());
        Package pack = packageService.getPackageById(packId);
        if(!pack.isTracking()){
            pack.setTracking(true);
            packageService.updatePackage(pack);
        }

        Checkpoint check = new Checkpoint(pack, checkpoint, new Date(System.currentTimeMillis()));
        packageService.addCheckpoint(check);
    }
}
