package soap;

import DAO.CheckpointDAOImp;
import DAO.PackageDAOImp;
import DAO.UserDAOImp;
import model.Checkpoint;
import model.Package;
import model.User;
import services.PackageService;
import services.PackageServiceImpl;
import services.UserService;
import services.UserServiceImpl;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "soap.ClientInterface")
public class ClientInterfaceImp implements ClientInterface {

    private UserService userService = new UserServiceImpl(new UserDAOImp());

    @Override
    public User getUserByCredentials(String username, String password) {
        User user = userService.getUserByCredentials(username,password);
        return user;
    }

    @Override
    public Package[] getUserPackages(User user) {
        if( !userService.isLoggedIn(user)) return new Package[0];
        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(), new CheckpointDAOImp());
        List<Package> packages = packageService.getUserPackages(user);
        return packages.toArray(new Package[packages.size()]);
    }

    @Override
    public Package getPackageById(Integer id, User user) {
        if( !userService.isLoggedIn(user)) return null;
        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(), new CheckpointDAOImp());
        Package pack =   packageService.getPackageById(id);
        if(user!=null && ( user.getUsername().equals(pack.getSender().getUsername())
                || user.getUsername().equals(pack.getReceiver().getUsername()) )){
            return pack;
        } else {
            return null;
        }
    }

    @Override
    public Checkpoint[] getPackageRoute(Integer packageId, User user) {
        if( !userService.isLoggedIn(user)) return new Checkpoint[0];
        PackageService packageService = new PackageServiceImpl(new PackageDAOImp(), new CheckpointDAOImp());
        Package pack = packageService.getPackageById(packageId);
        List<Checkpoint> checkpoints = packageService.getPackageCheckpoints(pack);
        return checkpoints.toArray(new Checkpoint[checkpoints.size()]);
    }

    @Override
    public User createUser(String username, String password) {
        UserService userService = new UserServiceImpl(new UserDAOImp());
        User u = new User();
        u.setUsername(username);
        u.setPassword(password);
        u.setAdmin(false);
        User user = userService.saveUser(u);
        return user;
    }

}

