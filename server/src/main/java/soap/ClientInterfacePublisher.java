package soap;

import javax.xml.ws.Endpoint;

public class ClientInterfacePublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8081/ws/client", new ClientInterfaceImp());
    }

}
