package soap;

import javax.xml.ws.Endpoint;

public class AdminInterfacePublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8082/ws/admin", new AdminInterfaceImp());
    }

}
