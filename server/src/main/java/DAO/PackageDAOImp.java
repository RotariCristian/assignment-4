package DAO;

import model.Package;
import model.User;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;


import java.util.ArrayList;
import java.util.List;

public class PackageDAOImp implements PackageDAO {

    private SessionFactory factory;

    public PackageDAOImp(){
        this.factory= new Configuration().configure().buildSessionFactory();
    }

    public Package addPackage(Package pack) {
        Session session = factory.openSession();
        Transaction tx = null;
        Package p=null;
        try {
            tx = session.beginTransaction();
            session.save(pack);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return p;
    }

    public void deletePackage(Package pack) {

        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(pack);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List<Package> getAllPackages() {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Package> packages = new ArrayList<>();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Package");
            packages = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return packages;
    }

    public List<Package> getClientPackages(User client) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Package> packages = new ArrayList<>();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Package where tracking = true and (receiver = :receiver or sender = :sender) ");
            query.setParameter("receiver", client );
            query.setParameter("sender", client );
            packages = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return packages;
    }

    public Package updatePackage(Package pack) {
        Session session = factory.openSession();
        Transaction tx = null;
        Package p=null;
        try {
            tx = session.beginTransaction();
            session.update(pack);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return p;
    }

    @Override
    public Package getPackageById(Integer id) {
        Session session = factory.openSession();
        Transaction tx = null;
        Package p=null;
        try {
            tx = session.beginTransaction();
            p = session.get(Package.class, id);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return p;
    }
}
