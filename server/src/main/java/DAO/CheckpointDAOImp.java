package DAO;

import model.Checkpoint;
import model.Package;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class CheckpointDAOImp  implements CheckpointDAO{


    private SessionFactory factory;

    public CheckpointDAOImp(){
        this.factory= new Configuration().configure().buildSessionFactory();
    }

    @Override
    public List<Checkpoint> getPackageCheckpoints(Package pack) {
        Session session = factory.openSession();
        Transaction tx = null;
        List<Checkpoint> checkpoints = new ArrayList<>();
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Checkpoint where aPackage = :package ");
            query.setParameter("package", pack );
            checkpoints = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return checkpoints;
    }

    @Override
    public void addCheckpoint(Checkpoint checkpoint) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(checkpoint);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public void deleteCheckpoint(Checkpoint checkpoint) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(checkpoint);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }


}
