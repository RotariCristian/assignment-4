package DAO;

import model.User;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class UserDAOImp implements UserDAO {

    private SessionFactory factory;

    public UserDAOImp(){
        this.factory= new Configuration().configure().buildSessionFactory();
    }

    public User getByCredentials(String username, String password) {
        Session session = factory.openSession();
        Transaction tx = null;
        User u=null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username and password = :password");
            query.setParameter("username", username );
            query.setParameter("password", password );
            List<User> result = query.list();
            if(result!=null && result.size()>0)
                u = result.get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return u;
    }

    public User saveUser(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            user.setId((int)session.save(user));
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
           user = null;
        } finally {
            session.close();
        }
        return user;
    }

    @Override
    public void updateUser(User user) {
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    @Override
    public User getUserByUsername(String username) {
        Session session = factory.openSession();
        Transaction tx = null;
        User u=null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username ");
            query.setParameter("username", username );
            List<User> result = query.list();
            if(result!=null && result.size()>0)
                u = result.get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return u;
    }

    @Override
    public User getById(Integer id) {
        Session session = factory.openSession();
        Transaction tx = null;
        User u=null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE id = :id ");
            query.setParameter("id", id );
            List<User> result = query.list();
            if(result!=null && result.size()>0)
                u = result.get(0);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return u;
    }
}
