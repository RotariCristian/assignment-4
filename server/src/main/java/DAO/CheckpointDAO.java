package DAO;

import model.Checkpoint;
import model.Package;

import java.util.List;

public interface CheckpointDAO {

    List<Checkpoint> getPackageCheckpoints(Package pack);

    void addCheckpoint(Checkpoint checkpoint);

    void deleteCheckpoint(Checkpoint checkpoint);
}
