package DAO;

import model.Package;
import model.User;

import java.util.List;

public interface PackageDAO {

    Package addPackage(Package pack);

    void deletePackage(Package pack);

    List<Package> getAllPackages();

    List<Package> getClientPackages(User client);

    Package updatePackage(Package pack);

    Package getPackageById(Integer id);
}
