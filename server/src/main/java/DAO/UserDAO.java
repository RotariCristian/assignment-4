package DAO;

import model.User;

public interface UserDAO {

    User getByCredentials(String username, String password);

    User saveUser(User user);

    void updateUser(User user);

    User getUserByUsername(String username);

    User getById(Integer id);


}
