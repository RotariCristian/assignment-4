package services;

import DAO.UserDAO;
import model.User;

import java.util.Random;

public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl(UserDAO userDAO){
        this.userDAO = userDAO;
    }

    @Override
    public User saveUser(User user) {
        Random rand = new Random();
        user.setToken(rand.nextInt(10000));
       return userDAO.saveUser(user);
    }

    @Override
    public User getUserByCredentials(String username, String password) {
        User u = userDAO.getByCredentials(username,password);
        if(u != null){
            Random rand = new Random();
            u.setToken(rand.nextInt(10000));
            this.updateUser(u);
        }
       return u;
    }

    @Override
    public User getUserById(Integer id) {
        return userDAO.getById(id);
    }

    @Override
    public User getUserByUsername(String username) {
        return userDAO.getUserByUsername(username);
    }

    @Override
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }

    @Override
    public boolean isLoggedIn(User user) {
        User u = getUserById(user.getId());
        if(u.getToken() == null || user.getToken() == null || !u.getToken().equals(user.getToken())) return false;
        return true;
    }
}
