package services;

import model.User;

public interface UserService
{
    User saveUser(User user);

    User getUserByCredentials(String username, String password);

    User getUserById(Integer id);

    User getUserByUsername(String username);

    void updateUser(User user);

    boolean isLoggedIn(User user);

}
