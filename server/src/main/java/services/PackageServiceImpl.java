package services;

import DAO.CheckpointDAO;
import DAO.PackageDAO;
import model.Checkpoint;
import model.Package;
import model.User;

import java.util.List;

public class PackageServiceImpl implements PackageService {

    private PackageDAO packageDAO;

    private CheckpointDAO checkpointDAO;

    public PackageServiceImpl(PackageDAO packageDAO, CheckpointDAO checkpointDAO) {
        this.packageDAO = packageDAO;
        this.checkpointDAO = checkpointDAO;
    }

    @Override
    public List<Package> getUserPackages(User user) {
        return this.packageDAO.getClientPackages(user);
    }

    @Override
    public Package getPackageById(Integer id) {
        return this.packageDAO.getPackageById(id);
    }

    @Override
    public List<Package> getAllPackages() {
        return packageDAO.getAllPackages();
    }

    @Override
    public void addPackage(Package pack) {
        packageDAO.addPackage(pack);
    }

    @Override
    public void deletePackageById(Integer id) {
        Package pack = packageDAO.getPackageById(id);
        List<Checkpoint> checks = checkpointDAO.getPackageCheckpoints(pack);
        for(Checkpoint check : checks){
            checkpointDAO.deleteCheckpoint(check);
        }
        packageDAO.deletePackage(pack);
    }

    @Override
    public Package updatePackage(Package pack) {

        return packageDAO.updatePackage(pack);
    }

    @Override
    public List<Checkpoint> getPackageCheckpoints(Package pack) {
        return checkpointDAO.getPackageCheckpoints(pack);
    }

    @Override
    public void addCheckpoint(Checkpoint checkpoint) {
        checkpointDAO.addCheckpoint(checkpoint);
    }

}