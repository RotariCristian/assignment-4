package services;

import model.Checkpoint;
import model.Package;
import model.User;

import java.util.List;

public interface PackageService {

    List<Package> getUserPackages(User user);

    Package getPackageById(Integer id);

    List<Package> getAllPackages();

    void addPackage(Package pack);

    void deletePackageById(Integer id);

    Package updatePackage(Package pack);

    List<Checkpoint> getPackageCheckpoints(Package pack);

    void addCheckpoint(Checkpoint checkpoint);


}
