
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Client</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-10">
            <h1>Client Page</h1>
<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Sender</th>
        <th>Receiver</th>
        <th>Sender City</th>
        <th>Destination City</th>
        <th>Check Status</th>
    </tr>
    </thead>
    <tbody>
    <div class="container-fluid">
    <c:forEach var ="pack" items ="${packageList}" >
        <tr>
            <tr>
                <td>${pack.name}</td>
                <td>${pack.description}</td>
                <td>${pack.sender.username}</td>
                <td>${pack.receiver.username}</td>
                <td>${pack.senderCity}</td>
                <td>${pack.destinationCity}</td>
                <td>
                    <a href="<c:url value='/client?action=status&id=${pack.id}' />">
                        <span class="	glyphicon glyphicon-time"></span>
                    </a>
                </td>
        </tr>
    </c:forEach>


        <br>
                <c:forEach var ="check" items ="${path}" >
                    <p> City: ${check.city}; Date: ${check.date}</p>
                </c:forEach>

        </div>
    </tbody>
</table>

        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>