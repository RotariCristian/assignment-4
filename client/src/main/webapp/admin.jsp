<%--
  Created by IntelliJ IDEA.
  User: Brazzar
  Date: 30.10.2017
  Time: 00:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


</head>
<body>

<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-10">
            <h1>Admin Page</h1>
<form action="${pageContext.request.contextPath}/admin" method="post">

<div class="form-group">
    <label class="control-label">Name</label>
    <input type="text" name="name" class="form-control input-sm chat-input" placeholder="Name" />
</div>

    <div class="form-group">
        <label class="control-label">Description</label>
        <input type="text" name="description" class="form-control input-sm chat-input" placeholder="Description" />
    </div>

    <div class="form-group">
        <label class="control-label">Sender City</label>
        <input type="text" name="senderCity" class="form-control input-sm chat-input" placeholder="Sender City" />
    </div>

    <div class="form-group">
        <label class="control-label">Destination City</label>
        <input type="text" name="destinationCity" class="form-control input-sm chat-input" placeholder="Destination City" />
    </div>

    <div class="form-group">
        <label class="control-label">Sender username</label>
        <input type="text" name="sender" class="form-control input-sm chat-input" placeholder="Sender" />
    </div>

    <div class="form-group">
        <label class="control-label">Receiver username</label>
        <input type="text" name="receiver" class="form-control input-sm chat-input" placeholder="Receiver" />
    </div>

    <input  class="btn success" type="submit" name="create" value="Create" />

</form>
            <br>

<form action="/admin" method="post">

    <select class="dropdown" name="item">
        <c:forEach var ="pack" items ="${packageList}" >
            <option class="dropdown-item" value="${pack.id}">${pack.name}</option>
        </c:forEach>
    </select>

    <div class="form-group">
        <label class="control-label">Current City</label>
        <input type="text" name="checkpoint" class="form-control input-sm chat-input" placeholder="CurrentCity" />
    </div>

    <input  class="btn danger" type="submit" name="location" value="Add Checkpoint" />
</form>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Sender</th>
        <th>Receiver</th>
        <th>Sender City</th>
        <th>Destination City</th>
        <th>Delete</th>
    </tr>
    </thead>
    <tbody>
    <div class="container-fluid">
        <c:forEach var ="pack" items ="${packageList}" >
            <tr>
            <tr>
                <td>${pack.name}</td>
                <td>${pack.description}</td>
                <td>${pack.sender.username}</td>
                <td>${pack.receiver.username}</td>
                <td>${pack.senderCity}</td>
                <td>${pack.destinationCity}</td>
            <td>
                <form action="/admin" method="post">
                    <input  class="btn danger" type="submit" name="delete" value="Delete" />
                    <input type="text" name="packId"  value = "${pack.id}" hidden />
                </form>
            </td>
            </tr>
        </c:forEach>
    </div>
    </tbody>
</table>
    </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>