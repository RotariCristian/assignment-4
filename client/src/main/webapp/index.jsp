<%--
  Created by IntelliJ IDEA.
  User: Brazzar
  Date: 30.10.2017
  Time: 00:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>login</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="login.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
            <div class="form-login">
            <h4>Package System Login</h4>

            <form action="${pageContext.request.contextPath}/login" method="post">
                <input type="text" name="username" class="form-control input-sm chat-input" placeholder="username" />
                </br>
                <input type="password" name="password" class="form-control input-sm chat-input" placeholder="password" />
                </br>
                <div class="wrapper">
            <span class="group-btn">
                 <input  class="btn btn-primary btn-md" type="submit" name="submit" value="Log In" />


            </span>
                </div>
            </form>

            <br>
            <p style="color: red">${error}</p>
        </div>
        </div>

            <div class="register">
            <div class="form-login">
                <h4>Package System Register</h4>

                <form action="${pageContext.request.contextPath}/login" method="post">
                    <input type="text" name="username1" class="form-control input-sm chat-input" placeholder="username" />
                    </br>
                    <input type="password" name="password1" class="form-control input-sm chat-input" placeholder="password" />
                    </br>
                    <input type="password" name="password2" class="form-control input-sm chat-input" placeholder="Confirm password" />
                    </br>
                    <div class="wrapper">
            <span class="group-btn">
                 <input  class="btn btn-primary btn-md" type="submit" name="register" value="Register" />


            </span>
                    </div>
                </form>

                <br>
                <p style="color: red">${error1}</p>
            </div>
            </div>



        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</body>
</html>
