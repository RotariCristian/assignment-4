package presentation;


import model.Checkpoint;
import model.Package;
import model.User;
import soap.ClientInterface;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

@WebServlet(urlPatterns = { "/client" })
public class ClientServlet extends HttpServlet {



    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        URL url = new URL("http://localhost:8081/ws/client?wsdl");

        QName qname = new QName("http://soap/", "ClientInterfaceImpService");
        Service service = Service.create(url, qname);
        ClientInterface clientService = service.getPort(ClientInterface.class);

        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");

        if(request.getParameter("action")!= null) {
            Package pack = clientService.getPackageById(Integer.parseInt(request.getParameter("id")), user);
            Checkpoint[] path = clientService.getPackageRoute(Integer.parseInt(request.getParameter("id")),user);
            request.setAttribute("path", path);
        }

        Package[] list = clientService.getUserPackages(user);
        request.setAttribute("packageList", list);
        request.getRequestDispatcher("client.jsp").forward(request,response);

    }

}
