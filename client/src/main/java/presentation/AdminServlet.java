package presentation;




import model.Package;
import model.User;
import soap.AdminInterface;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;


@WebServlet(urlPatterns = { "/admin" })
public class AdminServlet extends HttpServlet {


    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        URL url = new URL("http://localhost:8082/ws/admin?wsdl");

        QName qname = new QName("http://soap/", "AdminInterfaceImpService");
        Service service = Service.create(url, qname);
        AdminInterface adminService = service.getPort(AdminInterface.class);

        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");

        if(request.getParameter("action")!= null) {
//            Package pack = clientService.getPackageById(Integer.parseInt(request.getParameter("id")), user);
//            Checkpoint[] path = pack.getPath();
//
//            request.setAttribute("show", true);
//            if(pack!=null &&  path.length>0){
//                request.setAttribute("noPath", false);
//                request.setAttribute("path", path);
//            } else {
//                request.setAttribute("noPath", true);
//            }
        }

        Package[] list = adminService.getAllPackages(user);
        request.setAttribute("packageList", list);
        request.getRequestDispatcher("admin.jsp").forward(request,response);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        URL url = new URL("http://localhost:8082/ws/admin?wsdl");

        QName qname = new QName("http://soap/", "AdminInterfaceImpService");
        Service service = Service.create(url, qname);
        AdminInterface adminService = service.getPort(AdminInterface.class);

        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute("user");

        if ( request.getParameter("delete") != null){
            Integer packageId = Integer.parseInt(request.getParameter("packId"));
            adminService.deletePackage(packageId,user);

            response.sendRedirect("/admin");
            return;
        } else if(request.getParameter("location") != null) {

            Integer packId = Integer.parseInt(request.getParameter("item"));
            String checkpoint = request.getParameter("checkpoint");
            adminService.addCheckpoint(packId,checkpoint,user);

            response.sendRedirect("/admin");
            return;

        } else {

                String name = request.getParameter("name");
                String description = request.getParameter("description");
                String senderCity = request.getParameter("senderCity");
                String destinationCity = request.getParameter("destinationCity");
                String sender = request.getParameter("sender");
                String receiver = request.getParameter("receiver");

                adminService.createPackage(name,description,senderCity,destinationCity,sender,receiver,user);

                response.sendRedirect("/admin");
                return;
            }
        }


}
