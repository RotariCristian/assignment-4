package presentation;

import model.User;
import soap.ClientInterface;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.net.URL;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {


    public void deGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request,response);

    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        URL url = new URL("http://localhost:8081/ws/client?wsdl");

        QName qname = new QName("http://soap/", "ClientInterfaceImpService");
        Service service = Service.create(url, qname);
        ClientInterface clientService = service.getPort(ClientInterface.class);
        User user = null;

        if(request.getParameter("register") != null){
            String username = request.getParameter("username1");
            String password1 = request.getParameter("password1");
            String password2 = request.getParameter("password2");
            if(!password1.equals(password2)){
                request.setAttribute("error1", "Passwords do not match");
                request.getRequestDispatcher("index.jsp").include(request, response);
            } else {
                user = clientService.createUser(username, password1);

                if (user != null) {
                    HttpSession session = request.getSession(false);
                    session.setAttribute("user", user);
                    response.sendRedirect(request.getContextPath() + "/client");
                    return;
                    } else {
                    request.setAttribute("error1", "Username Taken");
                    request.getRequestDispatcher("index.jsp").include(request, response);
                }
            }
        } else {
            String username = request.getParameter("username");
            String password = request.getParameter("password");

            user = clientService.getUserByCredentials(username, password);


            if (user != null) {
                HttpSession session = request.getSession(false);
                session.setAttribute("user", user);
                if (user.isAdmin()) {
                    response.sendRedirect(request.getContextPath() + "/admin");
                    return;
                } else {
                    response.sendRedirect(request.getContextPath() + "/client");
                    return;
                }
            } else {
                request.setAttribute("error", "Invalid User");
                request.getRequestDispatcher("index.jsp").include(request, response);
            }
        }


    }

}
